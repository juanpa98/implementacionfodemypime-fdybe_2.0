export default {
    getAllEmpresas: 'SELECT * FROM Empresa',
    createNewEmpresa: 'INSERT INTO Empresa (nombreEmpresa, Contacto, Representante_legal, Direccion, Municipio,' +
        'Departamento, Aldea_Caserio, Telefono, Correo, Nit, fecha_inicioOperaciones ) VALUES (@nombreEmpresa,' + 
            '@Contacto, @Representante_legal, @Direccion, @Municipio, @Departamento, @Aldea_Caserio, @Telefono,' + 
            '@Correo, @Nit, @fecha_inicioOperaciones )'
}
