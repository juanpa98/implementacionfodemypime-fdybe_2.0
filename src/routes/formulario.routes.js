import { Router } from 'express' ;
import { createNewEmpresa, getOpEmpresa} from '../controllers/formulario.controller.js';

const router = Router()

router.get('/formulario', getOpEmpresa)


router.post('/formulario', createNewEmpresa )


export default router